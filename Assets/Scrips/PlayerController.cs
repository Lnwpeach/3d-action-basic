using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{//Control speed
	public float speed = 10;
    void FixedUpdate()
    {
        // substitution'input'into x and z
		float x = Input.GetAxis("Horizontal");
		float z = Input.GetAxis("Vertical");

		// acquire Rigidbody component which same Game object owned
		Rigidbody rigidbody = GetComponent<Rigidbody>();
		//add a power to x(side) and z(depth)in rigidbody
		//Multiply x and y by speed
		rigidbody.AddForce(x*speed,0,z*speed);


    }

    
    void Update()
    {
        
    }
}
